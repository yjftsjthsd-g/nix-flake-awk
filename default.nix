{ pkgs ? import <nixpkgs> {} }:

with pkgs;

stdenv.mkDerivation rec {
	name = "awk";
	#version = "4";
	src = fetchgit {
		url = "https://github.com/onetrueawk/awk.git";
		rev = "b92d8cecd132ce8e02a373e28dd42e6be34d3d59";
        hash = "sha256-sS6hX9cKUIjbZ8ZzGb+WYN4nIiJhuCkNRjdp6gbeSwo=";
	};

	buildInputs = [
		pkgs.coreutils
		pkgs.bison
	];
	# unpackPhase =
	# 	''
	# 	mkdir x farbfeld
	# 	cd farbfeld
	# 	tar --strip-components=1 -xzf $src
	# 	'';
	# buildPhase = "make PREFIX=$out";
	installPhase =
		''
		#make install PREFIX=$out
        mkdir -p $out/bin
        cp a.out $out/bin/awk
		'';

	meta = {
		description = "the one true awk";
		homepage = "https://github.com/onetrueawk/awk";
		#license = "custom";  # bsd-like
	};
}
