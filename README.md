# nix-flake-awk

A nix flake for awk.


## Use

On a flake-enabled nix system (see https://nixos.wiki/wiki/Flakes), either:

Clone this repo and
```
nix build
```

Or just build directly with
```
nix build gitlab:yjftsjthsd-g/nix-flake-awk
```


## License

The contents of this repo is under the ISC license (see LICENSE file), and the
awk is under what looks like a custom BSD-like license (see
https://github.com/onetrueawk/awk/blob/master/LICENSE)

